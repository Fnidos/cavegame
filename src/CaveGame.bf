using System;
using SDL2;
using System.Collections;

namespace CaveGame;

static
{
	public static CaveGame gGameApp;
}

class CaveGame : SDLApp
{
	public List<Entity> mEntities = new List<Entity>() ~ DeleteContainerAndItems!(_);
	public Hero mHero;
	public int mScore;
	public float mDifficulty;
	public Random mRand = new Random() ~ delete _;
#if !NOTTF
	Font mFont ~ delete _;
#endif
	float mBkgPos;
	int mEmptyUpdates;
	bool mHasMoved;
	bool mHasShot;
	bool mPaused;
	bool mDebugStringShow = false;
	bool mKeyAltDown = false;

	bool mRecolored = false;
	Random mRandom = new Random();

	public this()
	{
		gGameApp = this;

		mHero = new Hero();
		AddEntity(mHero);

		mHero.mY = 650;
		mHero.mX = 512;
	}

	public ~this()
	{
		Images.Dispose();
		Sounds.Dispose();
		delete mRandom;
	}

	public override void Init()
	{
		base.Init();
		SDL.SetWindowResizable(mWindow, true);
		Images.Init();
		if (mHasAudio)
			Sounds.Init();

		mFont = new Font();
		//mFont.Load("zorque.ttf", 24);
		mFont.Load("images/Main.fnt", 0);
	}

	public void DrawString(float x, float y, String str, SDL.Color color, bool centerX = false)
	{
		DrawString(mFont, x, y, str, color, centerX);
	}

	public override void Draw()
	{
		Draw(Images.sSpaceImage, 0, mBkgPos - 1024);
		Draw(Images.sSpaceImage, 0, mBkgPos);

		for (var entity in mEntities)
			entity.Draw();

		DrawString(8, 4, scope String()..AppendF("SCORE: {}", mScore), .(64, 255, 64, 255));

		if ((!mHasMoved) || (!mHasShot))
			DrawString(mWidth / 2, 200, "Use cursor keys to move and Space to fire", .(255, 255, 255, 255), true);

		if (mDebugStringShow) {
			DrawString(mWidth / 2, 300, "DebugString", .(255, 255, 255, 255), true);
		}
	}

	public void ExplodeAt(float x, float y, float sizeScale, float speedScale)
	{
		let explosion = new Explosion();
		explosion.mSizeScale = sizeScale;
		explosion.mSpeedScale = speedScale;
		explosion.mX = x;
		explosion.mY = y;
		mEntities.Add(explosion);
	}

	public void AddEntity(Entity entity)
	{
		mEntities.Add(entity);
	}

	public override void KeyDown(SDL.KeyboardEvent evt)
	{
		base.KeyDown(evt);
		if (evt.keysym.sym == .P)
			mPaused = !mPaused;
		else if (evt.keysym.sym == .LALT || evt.keysym.sym == .RALT)
			mKeyAltDown = true;
		else if ((mKeyAltDown && evt.keysym.sym == .RETURN) || evt.keysym.sym == .F) {
			mDebugStringShow = !mDebugStringShow;
			if (mDebugStringShow) {
				SDL.SetWindowFullscreen(mWindow, (.)SDL.WindowFlags.FullscreenDesktop);
			} else {
				SDL.SetWindowFullscreen(mWindow, 0);
			}
		} else if (evt.keysym.sym == .Q || evt.keysym.sym == .ESCAPE) {
			SDL.Event newEvent;
			newEvent.type = .Quit;
			SDL.PushEvent(ref newEvent);
		}
	}

	public override void KeyUp(SDL.KeyboardEvent evt)
	{
		base.KeyUp(evt);
		if (evt.keysym.sym == .LALT || evt.keysym.sym == .RALT)
			mKeyAltDown = false;
	}

	public override void HandleEvent(SDL.Event evt)
	{

		base.HandleEvent(evt);
		if (evt.type == .WindowEvent && evt.window.windowEvent == .Resized) {
			mWidth = evt.window.data1;
			mHeight = evt.window.data2;
		}

		else if (evt.type == .MouseButtonDown || evt.type == .MouseMotion) {

		}
	}

	void HandleInputs()
	{
		float deltaX = 0;
		float deltaY = 0;
		float moveSpeed = Hero.cMoveSpeed;
		if (IsKeyDown(.Left))
			deltaX -= moveSpeed;
		if (IsKeyDown(.Right))
			deltaX += moveSpeed;

		if (IsKeyDown(.Up))
			deltaY -= moveSpeed;
		if (IsKeyDown(.Down))
			deltaY += moveSpeed;

		if ((deltaX != 0) || (deltaY != 0))
		{
			mHero.mX = Math.Clamp(mHero.mX + deltaX, 10, mWidth - 10);
			mHero.mY = Math.Clamp(mHero.mY + deltaY, 10, mHeight - 10);
			mHasMoved = true;
		}
		mHero.mIsMovingX = deltaX != 0;

		if ((IsKeyDown(.Space)) && (mHero.mShootDelay == 0))
		{
			mHasShot = true;
			mHero.mShootDelay = Hero.cShootDelay;
			let bullet = new HeroBullet();
			bullet.mX = mHero.mX;
			bullet.mY = mHero.mY - 50;
			AddEntity(bullet);
			PlaySound(Sounds.sLaser, 0.1f);
		}
	}

	public override void Update()
	{
		if (mPaused)
			return;

		base.Update();

		HandleInputs();
		SpawnEnemies();

		// Make the game harder over time
		mDifficulty += 0.0001f;

		// Scroll the background
		mBkgPos += 0.6f;
		if (mBkgPos > 1024)
			mBkgPos -= 1024;

		for (var entity in mEntities)
		{
			entity.mUpdateCnt++;
			entity.Update();
			if (entity.mIsDeleting)
			{
				// '@entity' refers to the enumerator itself
                @entity.Remove();
				delete entity;
			}
		}
	}

	void SpawnEnemies()
	{
		bool hasEnemies = false;
		for (var entity in mEntities)
			if (entity is Enemy)
				hasEnemies = true;
		if (hasEnemies)
			mEmptyUpdates = 0;
		else
			mEmptyUpdates++;

		float spawnScale = 0.4f + (mEmptyUpdates * 0.025f);
		spawnScale += mDifficulty;

		if (mRand.NextDouble() < 0.002f * spawnScale)
			SpawnSkirmisher();

		if (mRand.NextDouble() < 0.0005f * spawnScale)
			SpawnGoliath();
	}

	void SpawnSkirmisher()
	{
		let spawner = new EnemySkirmisher.Spawner();
		spawner.mLeftSide = mRand.NextDouble() < 0.5;
		spawner.mY = ((float)mRand.NextDouble() * 0.5f + 0.25f) * mHeight;
		AddEntity(spawner);
	}

	void SpawnGoliath()
	{
		let enemy = new EnemyGolaith();
		enemy.mX = ((float)mRand.NextDouble() * 0.5f + 0.25f) * mWidth;
		enemy.mY = -300;
		AddEntity(enemy);
	}
}

